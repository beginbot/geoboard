package config

import (
	"fmt"
	"net"
	"os"
	"path"
	"path/filepath"
	"time"
)

// I am added options to this code for test only
// I want to be able to pass in a self-contained method
// ONLY for the tests
type Config struct {
	Channel   string
	nick      string
	Conn      net.Conn
	CoolDown  time.Duration
	SoundsDir string
}

func FindSoundsDir() string {
	homeDir, _ := os.Executable()
	newDir := path.Dir(homeDir)
	soundsDir := filepath.Join(newDir, "sounds")
	// fmt.Println("SoundsDir: ", soundsDir)
	return soundsDir
}

func New() Config {
	soundsDir := FindSoundsDir()
	oauth := findOauthToken()

  // conn io.ReadWriteCloser
	conn, err := net.Dial("tcp", "irc.chat.twitch.tv:6667")

	if err != nil {
		panic(fmt.Errorf("We got an error: %s", err))
	}

	nick := getenv("BOTNAME", "beginbotbot")
	channel := getenv("CHANNEL", "beginbot")
	coolDown := FindCoolDown()

	fmt.Fprintf(conn, "PASS %s\r\n", oauth)
	fmt.Fprintf(conn, "NICK %s\r\n", nick)
	fmt.Fprintf(conn, "JOIN #%s\r\n", channel)

	return Config{nick: nick, Channel: channel, Conn: conn, CoolDown: coolDown, SoundsDir: soundsDir}
}

func findOauthToken() string {
	oauth := os.Getenv("TWITCH_OAUTH_TOKEN")

	if oauth == "" {
		fmt.Println(
			`You must set the TWITCH_OAUTH_TOKEN Environment Variable.
      You can find one here: https://twitchapps.com/tmi/`)
		os.Exit(1)
	}

	return oauth
}

func getenv(envVar string, defaultValue string) string {
	value := os.Getenv(envVar)

	if value == "" {
		value = defaultValue
	}
	return value
}

func FindCoolDown() time.Duration {
	const DEFAULT_COOLDOWN_PERIOD = "60s"

	rawCoolDown := getenv("COOLDOWN_PERIOD", DEFAULT_COOLDOWN_PERIOD)
	coolDown, err := time.ParseDuration(rawCoolDown)

	if err != nil {
		errorMsg := fmt.Errorf("Invalid COOLDOWN_PERIOD %s. Error: %s", rawCoolDown, err)
		fmt.Println(errorMsg)
		coolDown, _ = time.ParseDuration(DEFAULT_COOLDOWN_PERIOD)
	}

	// fmt.Println("Cooldown Period: ", coolDown)
	return coolDown
}
