package soundboard

import (
	"github.com/veandco/go-sdl2/mix"
	"github.com/veandco/go-sdl2/sdl"
	"log"
	"path/filepath"
	"time"
)

func ProcessAudioQueue(audio_queue <-chan string, soundsDir string) {

  go func() {
    for audio_file := range audio_queue {
      playAudio(audio_file, soundsDir)
    }
  }()

}

func playAudio(audioFile string, soundsDir string) {
	mix.VolumeMusic(1)

	if err := sdl.Init(sdl.INIT_AUDIO); err != nil {
		log.Println(err)
		return
	}

	// Understand defer
	defer sdl.Quit()

	if err := mix.Init(mix.INIT_MP3); err != nil {
		log.Println(err)
		return
	}

	defer mix.Quit()

	if err := mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println(err)
		return
	}
	defer mix.CloseAudio()

	const VOLUME = 12
	mix.VolumeMusic(VOLUME)

	music, err := mix.LoadMUS(filepath.Join(soundsDir, audioFile))

	if err != nil {
		log.Println(err)
	}

	timer := time.NewTimer(5 * time.Second)
	ticker := time.NewTicker(100 * time.Millisecond)
	err = music.Play(1)

PlayingAudioLoop:
	for {
		<-ticker.C

		select {
		case <-timer.C:
			music.Free()
			break PlayingAudioLoop
		default:
		}

		if !mix.PlayingMusic() {
			music.Free()
			ticker.Stop()
			break
		}
	}

	if err != nil {
		log.Println(err)
	}
}
