package parser

import (
	"testing"
)

func TestParsePrivmsg(t *testing.T) {
	input_msg := ":gottzstreams!gottzstreams@gottzstreams.tmi.twitch.tv PRIVMSG #beginbot :nvim is embeddable so.. it doesn't even really matter where you embed. it's always using vimrc"

	user, _ := ParsePrivmsg(input_msg)

	if user != "gottzstreams" {
		t.Errorf("Did Not Parse Properly, user not found %s", user)
	}
}

