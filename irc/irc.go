package irc

import (
	"bufio"
	"fmt"
	"gitlab.com/beginbot/geoboard/config"
	"io"
)

func ReadIrc(reader io.Reader, done <-chan bool) <-chan string {
	messages := make(chan string)
	newreader := bufio.NewReader(reader)

  go func() {
    for {
      select {
        case <-done:
          return
        default:
      }

      res, _ := newreader.ReadString('\n')
      messages <- res
	  }
  }()

  return messages
}

func SendMsg(c config.Config, msg string) {
	fmt.Fprintf(c.Conn, "PRIVMSG #%s :%s\r\n", c.Channel, msg)
}

func Pong(c config.Config) {
	fmt.Println("We About to Pong!")
}
