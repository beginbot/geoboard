package outgoing

import (
	"fmt"
  "math"
	"io"
	"gitlab.com/beginbot/geoboard/config"
)

type MessageProcessor struct {
  Conn io.ReadWriter
  Channel string
}


func NewMessageProcessor(c config.Config) MessageProcessor {
  return MessageProcessor{Conn: c.Conn, Channel: c.Channel}
}


func (mp MessageProcessor) Process(messages <-chan string) {
  fmt.Println("Processing Time!")

  go func() {
    for msg := range messages {

      if len(msg) < 500 {
        fmt.Println("Msg: ", msg)
        fmt.Fprintf(mp.Conn, "PRIVMSG #%s :%s\r\n", mp.Channel, msg)
        continue
      }


      for {
        length := int(math.Min(float64(len(msg)), 500))
        head := msg[0:length]
        msg = msg[length:]
        fmt.Fprintf(mp.Conn, "PRIVMSG #%s :%s\r\n", mp.Channel, head)
        if len(msg) < 1 {
          break
        }
      }


    }

  }()



}
