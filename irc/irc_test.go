package irc

import (
	"bytes"
	"testing"
)

func TestReadIrc(t *testing.T) {
	connectionBuffer := bytes.NewBuffer([]byte{})
	connectionBuffer.WriteString("hello\n")

	messages := make(chan string)
	done := make(chan bool, 1)
	go ReadIrc(connectionBuffer, messages, done)
	done <- true
	res := <-messages
	if res != "hello\n" {
		t.Errorf("We did not get the expected message: %s", res)
	}
}
