# Geoboard

This project is intended to be a simple Twitch Chat soundboard bot,
for the game of [GeoGuessr](https://www.geoguessr.com/).

Chatters will be able to trigger Geo-specific sounds from the chat, to influence
the GeoGuessr Streamer.

Building and Running

```bash
go build main.go

./main


// You can Set a custom Cooldown Period
COOLDOWN_PERIOD=10s ./main

COOLDOWN_PERIOD=5m ./main
```

// ParseDuration parses a duration string.
// A duration string is a possibly signed sequence of
// decimal numbers, each with optional fraction and a unit suffix,
// such as "300ms", "-1.5h" or "2h45m".
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
