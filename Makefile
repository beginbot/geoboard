fmt:
	go fmt ./...

test:
	go test ./...

debug:
	go build -v
	# go build -gcflags=all="-N -l"
