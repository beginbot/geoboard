package router

import (
	"io/ioutil"
	"fmt"
	"gitlab.com/beginbot/geoboard/parser"
	"gitlab.com/beginbot/geoboard/config"
	"os"
	"os/exec"
	"strings"
	"time"
)

func Route(messages <-chan string) (<-chan string, <-chan string) {
	userActivity := map[string]time.Time{}
  soundsDir := config.FindSoundsDir()
	samples := sampleList(soundsDir)

  coolDown := config.FindCoolDown()

  audioQueue := make(chan string)
  outgoingQueue := make(chan string)

  go func()  {

    defer close(audioQueue)
    defer close(outgoingQueue)

	  for msg := range messages {

      if parser.IsPrivmsg(msg) {
        user, user_msg := parser.ParsePrivmsg(msg)

        if parser.IsCommand(user_msg) {
	        cmd := strings.ToLower(strings.TrimSpace(strings.Split(user_msg, " ")[0][1:]))
          fmt.Println("Command: ", cmd)

          if cmd == "color" {
            fmt.Println("CHANGING COLOR!!!")
            changeColorScheme(user_msg)
            continue
          }

          if cmd == "colors" {
            fmt.Println("Trying to Print the Colors")
            showColorOpts(outgoingQueue)
            continue
          }

           addToAudio(user, cmd, samples, audioQueue, userActivity, coolDown)
        } else {
          fmt.Println(user, ": ", user_msg)
        }

      } else if parser.IsPing(msg) {
        continue
        // irc.Pong(c)
      }
    }
  }()

  return audioQueue, outgoingQueue
}

func showColorOpts(outgoingQueue chan<-string) {
  outgoingQueue <-"https://github.com/dylanaraps/pywal/tree/master/pywal/colorschemes/light"
  outgoingQueue <-"https://github.com/dylanaraps/pywal/tree/master/pywal/colorschemes/dark"
}


func changeColorScheme(command string) {
	color := strings.TrimSpace(strings.Join(strings.Split(command, " ")[1:], " "))

	if color == "" {
		fmt.Println("Color: ", "random_dark")
		exec.Command("./colorscheme.sh", "random_dark").Output()
		return
	}

	fmt.Println("Color: ", color)
	exec.Command("./colorscheme.sh", color).Output()
}


func addToAudio(user string,
	cmd string,
	samples map[string]os.FileInfo,
	audioQueue chan<- string,
	userActivity map[string]time.Time,
	coolDown time.Duration) {

	result := samples[cmd]

	if result == nil {
		return
	} else {
		previousTime, inTheMap := userActivity[user]

		if inTheMap {
			t := time.Now()

			elapsed := previousTime.Sub(t)

			fmt.Println("Elapsed: ", elapsed)

			if elapsed > coolDown {
				fmt.Println(
					"You played a sound ", -elapsed, " seconds ago", " COOLDOWN_PERIOD: ", coolDown)
				return
			}
		}

		userActivity[user] = time.Now()
		audioQueue <- result.Name()
	}
}

func sampleList(soundsDir string) map[string]os.FileInfo {
	file_map := make(map[string]os.FileInfo)

	files, err := ioutil.ReadDir(soundsDir)

	if err != nil {
		return file_map
	}

	for _, file := range files {
		name := strings.TrimSpace(strings.Split(file.Name(), ".")[0])
		file_map[name] = file
	}

	return file_map
}

