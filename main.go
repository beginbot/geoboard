package main

import (
	"gitlab.com/beginbot/geoboard/config"
	"gitlab.com/beginbot/geoboard/irc"
	"gitlab.com/beginbot/geoboard/outgoing"
	"gitlab.com/beginbot/geoboard/router"
	"gitlab.com/beginbot/geoboard/soundboard"
)

func main() {
	c := config.New()

	done := make(chan bool, 1)

	messages := irc.ReadIrc(c.Conn, done)

	router.Route(messages)

	audioQueue, outgoingQueue := router.Route(messages)

	mp := outgoing.NewMessageProcessor(c)
	mp.Process(outgoingQueue)

	soundboard.ProcessAudioQueue(audioQueue, c.SoundsDir)

	<-done
}
