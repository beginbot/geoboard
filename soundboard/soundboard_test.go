package soundboard

import (
	// "bytes"
	"fmt"
	"testing"
)

func TestSampleList(t *testing.T) {
	file_map := SampleList("../sounds")
	fmt.Println(file_map)
	res := file_map["india"]

	if res == nil {
		t.Error("We did not find 'india' in the sample list!")
	}
}

func BenchmarkSampleList(b *testing.B) {
	file_map := SampleList("../sounds")
	res := file_map["india"]
	fmt.Println(res)
}
