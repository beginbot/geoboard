package outgoing

import (
  "testing"
  "gitlab.com/beginbot/geoboard/config"
)

func FakeMsgSender(messages <-chan string, msg string) {
}

func TestProcessMsg(t *testing.T) {
  // t.Error("We didn't process a message")

  messages := make(chan string, 1)
  messages <- "Cool Stuff"

	conn := bytes.Buffer(make([]byte,0))
  c := config.Config{Conn: conn, channel: "beginbotbot" }
  // if the message is greater than 500 length
  // We send multiple messages
  // is going to add messages to another channel
  ProcessMsg(c, messages, FakeMsgSender)

  // Generate a message beyond 500 in Length
  // messages <- "Cool Stuff"
}
